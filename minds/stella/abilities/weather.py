#!/usr/bin/env python

# weather.py - Retrieve Weather Forecast From Weatherbit (https://www.weatherbit.io)

import http.client

conn = http.client.HTTPSConnection("weatherbit-v1-mashape.p.rapidapi.com")

headers = {
    'content-type': "application/octet-stream",
    'X-RapidAPI-Key': "05f62d1375msh50d48ab79ff6b5ap169c2ejsnc8441f0adb4c",
    'X-RapidAPI-Host': "weatherbit-v1-mashape.p.rapidapi.com"
}

conn.request("GET", "/current?lon=38.5&lat=-78.5", headers=headers)

res = conn.getresponse()
data = res.read()

print(data.decode("utf-8"))





