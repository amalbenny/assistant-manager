#!/bin/bash

# speak.sh: Pipe Clipboard Through Voice Synthesis

xsel | flite -voice slt --setf int_f0_target_mean=263 --setf duration_stretch=1.25 --setf int_f0_target_stddev=35

# /usr/bin/festival --tts
