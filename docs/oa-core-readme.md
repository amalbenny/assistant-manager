## [Current Download Page](https://gitlab.com/open-assistant/oa-arch)

# Windows

**Experimental** "**exe**": http://openassistant.org/download/oa-alpha-windows-0.2.2.zip

Download, [unzip](http://www.peazip.org), and click "oa.exe" found within the "Open Assistant" folder.

### Basic System Dependencies

Install Python3: https://www.python.org/downloads/windows/

### Install Modules
  
 `pip3 install pywin32 keyboard sounddevice playsound requests pyttsx3 pocketsphinx psutil feedparser python-forecastio numpy`

### Other

Maybe use [a package manager](https://chocolatey.org)

* swig

# macOS

**Prerequisite**: [Homebrew](https://brew.sh)

### Dependencies:

Install Python3: `brew install python`

Install Python3 Dependencies: `pip3 install ${PYTHON_PKGS}`

### Modules:

**Audio Support** (gstreamer): `brew install gst-python --with-python --without-python@2`

Older versions of Homebrew require `--with-python3 --without-python`, check your version's formula options.

Support for autoaudiosrc: `brew install gst-plugins-good`

**Speech Recognition** (Sphinx):

_Build tools_: `brew install autoconf libtool automake swig`

_sphinxbase_

`git clone https://github.com/cmusphinx/sphinxbase.git`

`./autogen.sh && make install`

_pocketsphinx_

`git clone https://github.com/cmusphinx/pocketsphinx.git`

`./autogen.sh && make install`

# Linux

The package names may differ, the idea is the same..

* python3-dev
* libasound2-dev

## Ubuntu Linux:

`sudo apt-get install -y python python-dev python-pip build-essential swig git libpulse-dev espeak`

`pip3 install keyboard sounddevice playsound requests pyttsx3 pocketsphinx psutil feedparser python-forecastio numpy`

## Arch Linux:

`sudo pacman -S swig espeak`

`pip3 install keyboard sounddevice playsound requests pyttsx3 pocketsphinx psutil feedparser python-forecastio numpy`

