## Open Assistant - Arch Linux Version

Open Source Voice Assistant - Make Your Own Minds

## Description

Open Assistant is an offline open source voice assistant able to complete operating system tasks using spoken commands.

Speech recognition and language model updating operates locally, without internet access. 

Vocal commands and recordings never leave the machine. 

100% user privacy.

## Video Demonstrations

Version 0.00:
[https://youtu.be/-7Vh1ny9FsQ](https://youtu.be/-7Vh1ny9FsQ)

Version 0.11:
[https://youtu.be/_zBjn_LgiZM](https://youtu.be/_zBjn_LgiZM)

Version 0.21 with TP-Link Kasa LB130 and HS300:
[https://youtu.be/D99V9Ge9IaE](https://youtu.be/D99V9Ge9IaE)

## Installation

Make sure your system has been updated recently and you have `git` working.

Download the Open Assistant package, extract files, and launch the installation script:
```
wget https://gitlab.com/open-assistant/oa-arch/-/archive/main/oa-arch-main.zip && bsdtar xvf oa-arch-main.zip && mv -n oa-arch-main oa && cd oa && chmod 700 ./install.sh && ./install.sh;
```
KDE / Konsole users will also require ``qt-gstreamer``:
```
yay qt-gstreamer
```

## Usage

From within the Open Assistant directory, type:
```
./oa.sh
```
If you see **Stella: Listening**, Open Assistant is properly installed and running.

Congratulations. :)

Say **Hello Stella** and listen for a response.

If there is no response, be sure to check your microphone, audio settings, and speaker levels.

You can also try a recording test:
```
./mic-test.sh
```
User specific configuration is located in: [oa.sh](https://gitlab.com/open-assistant/oa-arch/-/blob/main/oa.sh)

Vocal commands and abilities are found in: [commands.json](https://gitlab.com/open-assistant/oa-arch/-/blob/main/minds/stella/abilities/commands.json)

After modifying `commands.json`, quit and relaunch `./oa.sh` to have changes made available.

## Support

Need help? Feel free to submit an issue or send an email:

[info@openassistant.org](mailto:info@openassistant.org)

## Contributing

Open Assistant is a unique and valuable project which would especially benefit anyone disabled, young, or elderly, as well as those of us to take a break from traditional mouse and keyboard, from time to time.

The combination of keyboard, mouse, and microphone is quite powerful.

Make computing more convenient, fun, private, and accessible!

Hack away, friends! :)

## Roadmap

Include a local language model toolkit, so Open Assistant will not require internet access to update commands. (COMPLETE!)

Offline speech dictation: https://github.com/ideasman42/nerd-dictation (Under current development: New demo video coming soon: https://www.youtube.com/c/andrewvavrek)

Map vocal commands to all available keyboard shortcuts and key characters. (Ongoing: https://gitlab.com/cunidev/gestures/-/wikis/xdotool-list-of-key-codes)

Introduce noise filtering / improve speech recognition. Whisper from OpenAI is a good candidate: https://openai.com/blog/whisper

Compile installation steps listed above into one ``install-arch.sh`` script, then create various install scrips for other distributions, such as ``install-debian.sh``, ``install-bsd.sh``, etc.

Generate specific command files for common Window Managers.

Create package installers and get Open Assistant into all popular repositories.

GUI design using Kivy.

Android app development.

## Authors and acknowledgment
Andrew Vavrek, Clayton Hobbs, George Antohi, Jezra, Jonathan Kulp, Ricky Houghton, Alex Hauptmann, Alexander Rudnick

## License
GPL V.3
